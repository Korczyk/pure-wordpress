var gulp = require('gulp');
var uglify = require('gulp-uglify');
var assetManifest = require('gulp-asset-manifest');
var sass = require('gulp-sass')(require('sass'));
var del = require('del');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
var concat = require('gulp-concat');
var watch = require('gulp-watch');
var merge = require('merge-stream');

var build = 'wp-content/themes/theme/public/build';
var assets = 'wp-content/themes/theme/assets';

var gulp = require('gulp');
gulp.task('hello', function() {
    console.log('Hello, World!');
});

gulp.task('base', function(){
    var  css = gulp.src([
        'node_modules/bootstrap-v4-rtl/dist/css/bootstrap.min.css',
    ])
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest(build+'/css'));

    var  js = gulp.src(['node_modules/jquery/dist/jquery.min.js', 'node_modules/bootstrap-v4-rtl/dist/js/bootstrap.min.js'])
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest(build+'/js'));

    return merge(css,js)
});

gulp.task('vendor', function(){
    var css = gulp.src([
        'node_modules/selectric/public/selectric.css',
        'node_modules/hamburgers/dist/hamburgers.css',
        // 'node_modules/jquery-confirm/dist/jquery-confirm.min.css',
    ])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 70 versions'))
        .pipe(concat('vendor.min.css'))
        .pipe(gulp.dest(build+'/css'))

    var js = gulp.src([
        'node_modules/selectric/public/jquery.selectric.min.js',
        // 'node_modules/jquery-confirm/dist/jquery-confirm.min.js'
    ])
        .pipe(concat('vendor.min.js'))
        .pipe(gulp.dest(build+'/js'))

    return merge(css,js)
});

gulp.task('js', function() {
    return gulp.src(assets+'/js/**/*.js')
        .pipe(uglify())
        .pipe(assetManifest({bundleName: 'manifest',manifestFile: 'manifest.json', pathPrepend: build+'/js'}))
        .pipe(gulp.dest(build+'/js'));
});

gulp.task('main-js', function() {
    return gulp.src(assets+'/js/main.min.js')
        .pipe(uglify())
        .pipe(gulp.dest(build+'/js'));
});

gulp.task('scss', function() {
    return gulp.src(assets+'/styles/scss/01_settings/settings.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 70 versions'))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest(build+'/css'));
});

gulp.task('clean', function() {
    return del([
        build+'/css/*',
        build+'/js/*',
    ]);
});

gulp.task('imagemin',function() {
    var image = gulp.src(assets+'/images/*.*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(build+'/images/system'));
    var tiny = gulp.src(assets+'/images/tiny/*')
        .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 80, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
        .pipe(gulp.dest(build+'/images/tiny'));

    return merge(image,tiny)
})


gulp.task('watch', function(){
    gulp.watch(assets+'/styles/scss/**/*.scss', gulp.series(['scss']));
    // gulp.watch(assets+'/js/**/*.js', gulp.series('js'));
    gulp.watch(assets+'/js/main.min.js', gulp.series(['main-js']));
});
// gulp.task('all', gulp.series(['clean', 'styles','uglify']));
gulp.task('all', gulp.series(['clean','base', 'scss','js', 'watch']));
