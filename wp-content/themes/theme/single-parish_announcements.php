<?php
/**
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template__pt-parish_announcements">
    <div class="guide-post">
        <div class="container container__vertical-margin container__margin-top-0 container-content-grey">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
            <div class="row">
                <div class="col">
                    <div class="guide-post__wrapper">
                        <div class="guide-post__content" itemprop="articleBody">
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                <h1 class="pt__header header-1 text-center header--bg"><?php echo get_the_title(); ?></h1>
                                <div class="pt__text box--grey wysiwyg">
                                    <?php echo get_field('text'); ?>
                                </div>
                                <?php
                                $files = get_field('files');
                                if($files): ?>
                                    <div class="video files">
                                        <h2 class="header-1 video__header file__header">Inne załączniki</h2>
                                        <?php get_template_part('template-parts/custom/files', null, ['files'=>$files]); ?>
                                    </div>
                                <?php endif; ?>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>