<?php
/**
 *
 * Template name: Szablon Bazylika
 *
 * @package storefront
 */
get_header(); ?>
    <div class="template" id="template_basilica">
        <div class="container">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
            <?php get_template_part('template-parts/page-title'); ?>

            <div class="row">
                <div class="col-lg-5 col-991-padding-auto-0">
                    <?php $image = get_field('image');?>
                    <img class="lazy main-image" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo isset($image['url'])?$image['url']:null; ?>" alt="image">
                    <br />
                </div>
                <div class="col-lg-7 col-991-padding-auto-0">
                    <?php
                    $class = isset($args['class'])&&$args['class']!=''?$args['class']:'header--no-bg';
                    $key = isset($args['key'])&&$args['key']!=''?$args['key']:'info';
                    $content = get_field($key);

                    foreach ($content as $row) {
                        if (isset($row['trigger']) && $row['trigger']) {
                            $header = $row['header'];
                            $text = $row['text'];
                            $files = $row['files']; ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files, 'class'=>$class]); ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php $timeline = get_field('about');?>
        <?php if(isset($timeline['trigger'])&&$timeline['trigger']): ?>
            <?php get_template_part('template-parts/homepage/timeline',null,['key'=>$timeline]); ?>
        <?php endif; ?>
    </div>
<?php get_footer(); ?>