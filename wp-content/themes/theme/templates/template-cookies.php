<?php
/**
 *
 * Template name: Szablon Cookies
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_cookies">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'info';
        $content = get_field($key);

        foreach ($content as $row) {
            if (isset($row['trigger']) && $row['trigger']) {
                $header = $row['header'];
                $text = $row['text'];
                $files = $row['files']; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files, 'class'=>$class]); ?>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>
