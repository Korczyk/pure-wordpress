<?php
/**
 *
 * Template name: Szablon Strona VR
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_in-build">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
            <div class="embed-responsive embed-responsive-21by9">
                <div id="vr"></div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
