<?php
/**
 *
 * Template name: Szablon Galerie
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_gallery">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <?php get_template_part('template-parts/lightbox/gallery'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>