<?php
/**
 *
 * Template name: Szablon Instytucje
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_instytutions">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <?php get_template_part('template-parts/instytutions/instytution'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
