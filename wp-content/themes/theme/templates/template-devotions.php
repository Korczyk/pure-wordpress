<?php
/**
 *
 * Template name: Szablon Nabożeństwa
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_devotions">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
    </div>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'holy_mass']); ?>
    <?php get_template_part('template-parts/devotions/table',null,['key'=>'penance_and_reconciliation']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'penance_and_reconciliation']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'week_devotion']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'month_devotion']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'periodic_devotion']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'catechesis']); ?>
    <?php get_template_part('template-parts/devotions/common-list-elem',null,['key'=>'indulgences']); ?>
</div>
<?php get_footer(); ?>
