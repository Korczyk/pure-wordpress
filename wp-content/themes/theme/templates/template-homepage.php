<?php
/**
 *
 * Template name: Szablon Strona Główna
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_homepage">
    <?php /*Banner główny*/ ?>
    <?php get_template_part('template-parts/homepage/banner'); ?>
    <?php /*Intencje mszalne, ogłoszenie duszpasterskie*/ ?>
    <?php get_template_part('template-parts/homepage/vertical-timeline'); ?>
    <?php /*Cytat*/ ?>
    <?php get_template_part('template-parts/homepage/quote'); ?>
    <?php /*O nas*/ ?>
    <?php get_template_part('template-parts/homepage/about'); ?>
    <div id="main-map" class="map"></div>
    <?php /*Ewangelizacja*/ ?>
    <?php /*get_template_part('template-parts/widgets/gospel-opoka');*/ ?>
</div>
<?php get_footer(); ?>