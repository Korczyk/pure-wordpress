<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Pielgrzymuj z nami
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-with-us">
    <div class="container-fluid">
        <div class="container">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
            <?php get_template_part('template-parts/page-title'); ?>
        </div>

        <div class="container">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active header-3" data-toggle="tab" href="#sign-up">Zapisz się</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link header-3" data-toggle="tab" href="#pilgrimage-program">Program</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link header-3" data-toggle="tab" href="#route">Trasa</a>
                </li>
            </ul>
        </div>


        <div class="tab-content">
            <div id="sign-up" class="tab-pane active">
                <div class="container">
                    <?php
                    $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
                    $key = isset($args['key'])&&$args['key']!=''?$args['key']:'sign_up';
                    $content = get_field($key);

                    foreach ($content['info'] as $row) {
                        if (isset($row['trigger']) && $row['trigger']) {
                            $header = $row['header'];
                            $text = $row['text'];
                            $files = $row['files']; ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files]); ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div id="pilgrimage-program" class="tab-pane fade">
                <div class="container">
                    <?php
                    $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
                    $key = isset($args['key'])&&$args['key']!=''?$args['key']:'pilgrimage_program';
                    $content = get_field($key);

                    foreach ($content['day'] as $row) {
                        $header = $row['header'];
                        $text = $row['program']; ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text]); ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div id="route" class="tab-pane fade">
                <div class="row">
                    <div class="col">
                        <div id="template_homepage">
                            <div id="route-map" class="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
