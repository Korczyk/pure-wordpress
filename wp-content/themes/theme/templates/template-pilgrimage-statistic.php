<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Statystyki
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-statistic">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'statistics';
        $content = get_field($key); ?>


        <div class="row">
            <div class="col-lg-6 col-md-6">
                <ul class="nav flex-column" role="tablist">
                    <?php $i=0;?>
                    <?php foreach ($content['info'] as $row): ?>
                        <?php $title = $row['header']; ?>
                        <li class="nav-item">
                            <a class="nav-link header-3 btn-to-elem <?php if($i==0): ?>active<?php endif; ?>" data-toggle="tab"  href="#song-<?php echo $i; ?>"><?php echo isset($title)&&$title!=''?$title:null; ?></a>
                        </li>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="tab-content">
                    <?php $i=0;?>
                    <?php foreach ($content['info'] as $row): ?>
                        <?php $text = $row['text']; ?>
                        <div id="song-<?php echo $i; ?>" class="tab-pane <?php if($i==0): ?>active<?php else: ?>fade<?php endif; ?>">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['text' => $text]); ?>
                                </div>
                            </div>
                        </div>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
