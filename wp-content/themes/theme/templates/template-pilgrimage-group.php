<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Grupa pokutna
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-group">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'group';
        $content = get_field($key);

        foreach ($content['info'] as $row) {
            $header = $row['header'];
            $text = $row['text'];
            $files = $row['files']; ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files]); ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>
