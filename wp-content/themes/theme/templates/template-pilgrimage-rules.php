<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Przygotowanie i zasady
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-vademecum">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active header-3" data-toggle="tab" href="#vademecum">Jak się przygotować</a>
            </li>
            <li class="nav-item">
                <a class="nav-link header-3" data-toggle="tab" href="#rules">Regulamin</a>
            </li>
        </ul>

        <div class="tab-content">
            <div id="vademecum" class="tab-pane active">
                <?php
                $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
                $key = isset($args['key'])&&$args['key']!=''?$args['key']:'vademecum';
                $content = get_field($key);

                foreach ($content['info'] as $row) {
                    if (isset($row['trigger']) && $row['trigger']) {
                        $header = $row['header'];
                        $text = $row['text'];
                        $files = $row['files']; ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files]); ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div id="rules" class="tab-pane fade">
                <?php
                $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
                $key = isset($args['key'])&&$args['key']!=''?$args['key']:'rules';
                $content = get_field($key);

                foreach ($content['info'] as $row) {
                    $header = $row['header'];
                    $text = $row['text'];
                    $files = $row['files']; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files]); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>


    </div>
</div>
<?php get_footer(); ?>
