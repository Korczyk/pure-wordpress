<?php
/**
 *
 * Template name: Szablon Dzwony
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_bells">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'info';
        $content = get_field($key);
        $counter = 0;

        foreach ($content as $row) {
            if (isset($row['trigger']) && $row['trigger']) {
                $header = $row['header'];
                $text = $row['text'];
                $files = $row['files'];
                $images = $row['images']; ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text, 'files' => $files, 'class'=>$class]); ?>
                        <ul class="row" id="single-lightbox">
                            <?php
                            foreach ($images as $image): ?>
                                <?php $description = $image['description']; ?>
                                <?php $image = $image['image']; ?>
                                <?php if(isset($image['url'])&&$image['url']): ?>
                                    <li class="col-lg-3 col-md-4 col-sm-6 slide">
                                        <figure class="box-shadow">
                                            <a data-lightbox="gallery-<?php echo $counter; ?>" href="<?php echo isset($image['url'])&&$image['url']?$image['url']:get_events_image(); ?>" data-title="<?php echo isset($description)&&$description?$description:null; ?>">
                                                <img class="lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo $image['url']; ?>" alt="<?php echo isset($image['alt'])&&$image['alt']?$image['alt']:'pilgrimage-image'; ?>"  data-title="<?php echo isset($description)&&$description?$description:null; ?>">
                                            </a>
                                        </figure>
                                    </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <?php $counter++; ?>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>
