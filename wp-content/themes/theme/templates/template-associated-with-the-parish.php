<?php
/**
 *
 * Template name: Szablon Związani z parafią
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_associated-with">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <?php /*
        <?php get_template_part('template-parts/associated-with/parish-priest'); ?>
        <?php get_template_part('template-parts/associated-with/priests'); ?>
        */ ?>
        <?php get_template_part('template-parts/parish/priests',null,['key'=>'other_priests']); ?>
        <?php get_template_part('template-parts/associated-with/workers'); ?>
        <?php get_template_part('template-parts/associated-with/associated'); ?>
    </div>
</div>
<?php get_footer(); ?>
