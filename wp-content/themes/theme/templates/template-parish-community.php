<?php
/**
 *
 * Template name: Szablon Wspólnoty parafialne
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_parish-community">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <div class="col">
                <?php get_template_part('template-parts/parish-community/community'); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
