<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Pamiętnik pielgrzyma
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-diary">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <div class="col-lg-8">
                <?php

                    $comments_per_page = 10;
                    $id = get_the_ID();
                    $all_comments = wp_count_comments($id);
                    $all_comments_approved = $all_comments->approved;
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $offset = (($paged-1) * $comments_per_page) ;
                    $max_num_pages = ceil( $all_comments_approved / $comments_per_page );

                    $comments = get_comments(array(
                        'post_id' =>$id,
                        'orderby' => 'post_date',
                        'status' => 'approve',
                        'number' => $comments_per_page,
                        'offset' => $offset,
                    )); ?>
                    <div class="comment-box">
                    <?php
                    foreach ($comments as $comment) { ?>
                        <div class="comment">
                            <div class="left-side"><?php echo get_avatar( $comment, 45 ); ?></div>
                            <div class="right-side">
                                <span class="comment__author"><?php echo $comment->comment_author; ?></span>
                                <span class="comment__date"><?php echo get_comment_date('Y-m-d'); ?></span>
                                <p class="comment__text"><?php echo $comment->comment_content; ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                    <?php
                    $current_page = max(1, get_query_var('paged'));
                    echo paginate_links(array(
                        'base' => get_pagenum_link(1) . '%_%',
                        'type' => 'list',
                        'current' => $current_page,
                        'total' => $max_num_pages,
                        'end_size' => 0,
                        'mid_size' => 0,
                    ));
                ?>
            </div>
            <div class="col-lg-4">
                <?php comment_form(); ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
