<?php
/**
 *
 * Template name: Szablon Miejscowości i ulice
 *
 * @package storefront
 */
get_header(); ?>
    <div class="template" id="template_localization">
        <div class="container">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
            <?php get_template_part('template-parts/page-title'); ?>
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
                        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'info';
                        $content = get_field($key);
                        $header = $content['header'];
                        $text = $content['text'];
                    ?>
                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text,'class'=>$class]); ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>