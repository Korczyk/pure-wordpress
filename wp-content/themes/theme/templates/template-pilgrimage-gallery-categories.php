<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Galerie pielgrzymkowe (kategorie)
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_gallery-categories">
    <div class="container archive">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row list-of-elem">
            <?php

            $args = array(
                'post_type'      => 'page',
                'posts_per_page' => -1,
                'post_parent'    => $post->ID,
                'orderby' => 'date',
                'orderby'   => array(
                    'date' =>'DESC',
                ),
            );
            $parent = new WP_Query( $args );

            if ( $parent->have_posts() ) : ?>
                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="box-shadow">
                            <a class="elem" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
                                <?php if(has_post_thumbnail()): ?>
                                    <img class="elem__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                                <?php else: ?>
                                    <img class="elem__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo get_events_image(); ?>" alt="image">
                                <?php endif; ?>
                                <h2 class="elem__header header-2"><?php the_title(); ?></h2>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>