<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Pielgrzymka Sieradzka
 *
 * @package storefront
 */
get_header(); ?>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v12.0" nonce="P30hvapg"></script>
<div class="template template_pilgrimage" id="template_pilgrimage">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <?php
            $content = get_fields();

            $header = $content['header'];
            $text = $content['text'];

            $facebook_header = $content['facebook_data']['header'];
            $facebook_url = $content['facebook_data']['facebook_url'];

            $mixcloud_header = $content['mixcloud_data']['header'];
            ?>
            <div class="col-lg-8 order-1 order-md-0">
                <div class="text-box">
                    <h2 class="header-2"><?php echo isset($header)&&$header!=''?$header:null; ?></h2>
                    <div class="box--grey">
                        <div class="pt__text wysiwyg">
                            <?php echo isset($text)&&$text!=''?$text:null; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 order-0 order-md-1">
                <?php if(isset($content['mixcloud_data']['trigger']) && $content['mixcloud_data']['trigger']): ?>
                <div class="mixcloud-box">
                    <h2 class="header-2"><?php echo isset($mixcloud_header)&&$mixcloud_header!=''?$mixcloud_header:null; ?></h2>
                    <iframe width="100%" height="120" src="https://www.mixcloud.com/widget/iframe/?hide_cover=1&feed=%2Fpielgrzymkasieradzka%2Fjak-dawniej-pielgrzymowano%2F" frameborder="0" ></iframe>
                </div>
                <?php endif; ?>
                <?php if(isset($content['facebook_data']['trigger']) && $content['facebook_data']['trigger']): ?>
                <div class="facebook-box">
                    <h2 class="header-2"><?php echo isset($facebook_header)&&$facebook_header!=''?$facebook_header:null; ?></h2>
                    <div class="fb-timeline-box">
                        <div class="fb-page" data-href="<?php echo isset($facebook_url)&&$facebook_url!=''?$facebook_url:acf_get_field($key)['sub_fields'][0]['default_value']; ?>" data-tabs="timeline,events,messages" data-width="600" data-height="" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                            <blockquote cite="<?php echo isset($facebook_url)&&$facebook_url!=''?$facebook_url:acf_get_field($key)['sub_fields'][0]['default_value']; ?>" class="fb-xfbml-parse-ignore">
                                <a href="<?php echo isset($facebook_url)&&$facebook_url!=''?$facebook_url:acf_get_field($key)['sub_fields'][0]['default_value']; ?>">
                                    <div class="iframe-plug">
                                        <img src="<?php echo get_loader_gif(); ?>" alt="image">
                                        <span class="iframe-label">Ładowanie aktualności...</span>
                                    </div>
                                </a>
                            </blockquote>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
