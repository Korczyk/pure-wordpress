<?php
/**
 *
 * Template name: Szablon Kancelaria
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_office">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
    </div>
    <?php get_template_part('template-parts/office/main-info'); ?>
    <?php get_template_part('template-parts/devotions/table',null,['key'=>'office']); ?>
    <?php get_template_part('template-parts/office/common-description',null,['key'=>'holy_baptism']); ?>
    <?php get_template_part('template-parts/office/common-description',null,['key'=>'sacrament_of_marriage']); ?>
</div>
<?php get_footer(); ?>
