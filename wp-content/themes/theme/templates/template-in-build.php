<?php
/**
 *
 * Template name: Szablon Strona w budowie
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_in-build">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>
        <div class="row">
            <img src="<?php echo get_in_build_image(); ?>" alt="image">
        </div>
    </div>
</div>
<?php get_footer(); ?>
