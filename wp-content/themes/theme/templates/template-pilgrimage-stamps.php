<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Znaczki pielgrzymki sieradzkiej
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-stamps">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'description';
        $content = get_field($key); ?>

        <div class="row">
            <div class="col-lg-12">
            <?php foreach ($content['info'] as $row) {
                $header = $row['header'];
                $text = $row['text']; ?>
                    <?php get_template_part('template-parts/pilgrimage/common-section', null, ['header' => $header, 'text' => $text]); ?>
            <?php } ?>
            </div>
            <ul class="row" id="single-lightbox">
                <?php
                $images = get_field('gallery');
                $images = $images[0]['images'];
                $counter = 0;
                foreach ($images as $image): ?>
                    <?php $description = $image['description']; ?>
                    <?php $image = $image['image']; ?>
                    <?php if(isset($image['url'])&&$image['url']): ?>
                        <li class="col-lg-3 col-md-4 col-sm-6 slide">
                            <figure class="box-shadow">
                                <a data-lightbox="gallery-<?php echo $counter; ?>" href="<?php echo isset($image['url'])&&$image['url']?$image['url']:get_events_image(); ?>" data-title="<?php echo isset($description)&&$description?$description:null; ?>">
                                    <img class="lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo $image['url']; ?>" alt="<?php echo isset($image['alt'])&&$image['alt']?$image['alt']:'pilgrimage-image'; ?>"  data-title="<?php echo isset($description)&&$description?$description:null; ?>">
                                </a>
                            </figure>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>

<!--            --><?php //get_template_part('template-parts/lightbox/gallery'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
