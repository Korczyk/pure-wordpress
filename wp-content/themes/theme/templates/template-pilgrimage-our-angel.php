<?php
/**
 *
 * Template name: Szablon PIELGRZYMKA - Nasz Anioł
 *
 * @package storefront
 */
get_header(); ?>
<div class="template template_pilgrimage" id="template_pilgrimage-our-angel">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <?php get_template_part('template-parts/page-title'); ?>

        <?php
        $class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
        $key = isset($args['key'])&&$args['key']!=''?$args['key']:'statistics';
        $content = get_field($key);
        ?>

        <div class="row angel">
            <div class="col-lg-3 col-md-4">
                <?php if(has_post_thumbnail()): ?>
                    <img class="angel__image" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                <?php else: ?>
                    <img class="angel__image" src="<?php echo get_events_image(); ?>" alt="image">
                <?php endif; ?>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="wysiwyg">
                    <?php echo get_field('text'); ?>
                </div>
            </div>
        </div>
        <?php get_template_part('template-parts/custom/attachements'); ?>
    </div>
</div>
<?php get_footer(); ?>
