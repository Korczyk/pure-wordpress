<?php
/*
function get_arrange_consultation_shortcode($atts) {

    $title = $atts['title']!=''?$atts['title']:acf_get_field('arrange_consultation_shortcode_title')['default_value'];
    $subtitle = $atts['subtitle']!=''?$atts['subtitle']:acf_get_field('arrange_consultation_shortcode_subtitle')['default_value'];
    $cta = $atts['cta']!=''?$atts['cta']:acf_get_field('arrange_consultation_shortcode_cta')['default_value'];

    ob_start(); ?>
    <div class="arrange-consultation-shortcode <?php if($atts['bg']!=''):?>is-background-image<?php endif; ?>" <?php if($atts['bg']!=''):?>style="background-image: url(<?php echo $atts['bg']; ?>)"<?php endif; ?>>
        <h2 class="arrange-consultation-shortcode__title section-header"><?php echo $title ?></h2>
        <p class="arrange-consultation-shortcode__subtitle section-content"><?php echo $subtitle ?></p>
        <button type="button" class="arrange-consultation-shortcode__cta btn btn-green" data-toggle="modal" data-target="#widgetArrangeConsultation"><?php echo $cta ?></button>
    </div>
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}
add_shortcode('get_arrange_consultation', 'get_arrange_consultation_shortcode');
*/

function get_images_dir() {
    return get_site_url().'/wp-content/themes/kolegiata-theme/assets/images';
}
/*
function get_parish_announcement_image() {
    ob_start(); ?>
    <img src="<?php echo get_images_dir();?>/ogloszenia-duszpasterskie.jpg" alt="image">
    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}
*/

function get_parish_announcement_image() {
    return get_images_dir()."/ogloszenia-duszpasterskie.jpg";
}

function get_in_build_image() {
    return get_images_dir()."/strona-w-budowie.jpg";
}

function get_mass_intention_image() {
    return get_images_dir()."/intencje-mszalne.jpg";
}

function get_events_image() {
    return get_images_dir()."/empty.jpg";
}

function get_pdf_image() {
    return get_images_dir()."/pdf-icon.png";
}

function get_blank_gif() {
    return get_images_dir()."/blank.gif";
}

function get_loader_gif() {
    return get_images_dir()."/loading.gif";
}

function get_play_image() {
    return get_images_dir()."/play.png";
}

function get_facebook() {
    return "https://www.facebook.com/Bazylika.Sieradzka/";
}

function get_youtube() {
    return "https://www.youtube.com/channel/UCItGIzP7BI4qAPOVUnA8Arg";
}

function get_cat_to_top() {
    return get_images_dir()."/top.png";
}

function get_cat_to_right() {
    return get_images_dir()."/next.png";
}