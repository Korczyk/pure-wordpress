<?php
function explode_url() {
    $str = add_query_arg( NULL, NULL );
    return array_filter(explode('/',$str), function($value) { return !is_null($value) && $value !== ''; });
}
