<?php
/**
 *
 *
 * @package storefront
 */
get_header(); ?>
    <div class="template" id="template__audiotec">
        <div class="guide-post">
            <div class="container container-content-grey">
                <?php get_template_part('template-parts/breadcrumbs'); ?>
                <div class="row">
                    <div class="col d-flex">
                        <div class="guide-post__wrapper">
                            <div class="guide-post__content" itemprop="articleBody">
                                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                    <h1 class="pt__header header-1 text-center header--bg"><?php echo get_the_title(); ?></h1>
                                    <div class="pt__text wysiwyg">
                                        <?php echo get_field('text'); ?>
                                    </div>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php get_template_part('template-parts/custom/attachements'); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>