<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$header = isset($args['header'])&&$args['header']!=''?$args['header']:null;
$text = isset($args['text'])&&$args['text']!=''?$args['text']:null;
$files = isset($args['files'])&&$args['files']!=''?$args['files']:null;
?>
<?php if(isset($header)&&$header!=''): ?>
    <h2 class="header-2 header--bg <?php echo $class; ?>"><?php echo $header; ?></h2>
<?php endif; ?>
<?php if(isset($text)&&$text!=''): ?>
    <div class="description wysiwyg"><?php echo $text; ?></div>
<?php endif; ?>
<?php
if(isset($files)&&sizeof($files)): ?>
    <?php get_template_part('template-parts/custom/files', null, ['files'=>$files]); ?>
<?php endif; ?>