<?php
$gallery = get_field('gallery');
$links = get_field('links');
$files = get_field('files');
?>
<?php if($gallery || $links || $files): ?>
    <div class="row attachements">
        <?php if(!empty($gallery)): ?>
            <?php get_template_part('template-parts/lightbox/gallery'); ?>
        <?php endif; ?>
        <?php if(!empty($links)): ?>
            <div class="col-lg-4 col-md-6">
                <div class="section">
                    <h2 class="header-1 section__header">Załączniki video</h2>
                    <?php
                    foreach ($links as $yt):
                        $id = $yt['id_yt'];
                        if($id):?>
                            <?php get_template_part('template-parts/custom/handler-yt',null,['id'=>$id]); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php get_template_part('template-parts/custom/modal-yt'); ?>
                </div>
            </div>
        <?php endif; ?>
        <?php if(!empty($files)): ?>
            <div class="col-lg-4 col-md-6">
                <div class="section files">
                    <h2 class="header-1 section__header">Inne załączniki</h2>
                    <?php get_template_part('template-parts/custom/files', null, ['files'=>$files]); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
<?php endif ;?>