<div class="modal fade" id="modal-yt" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="icon">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <div class="iframe-plug">
                <img src="<?php echo get_loader_gif(); ?>" alt="image">
                <span class="iframe-label">Ładowanie filmu...</span>
            </div>
        </div>
    </div>
  </div>
</div>