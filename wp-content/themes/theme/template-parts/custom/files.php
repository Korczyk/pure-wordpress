<?php
$files = isset($args['files'])&&$args['files']!=''?$args['files']:null;
?>
<div class="files">
    <?php foreach ($files as $file):
        $file = $file['file'];
        $type = $file['mime_type'];
        if($file!=""):
            if($type == 'application/pdf'): ?>
            <a href="<?php echo $file['url'];?>" class="file" download>
                <img src="<?php echo get_pdf_image(); ?>" alt="pdf-icon" class="file__icon">
                <span class="file__title"><?php echo $file['title']; ?></span>
            </a>
            <?php elseif ($type == 'audio/mpeg'): ?>
            <div class="file">
                <audio controls>
                    <source src="<?php echo $file['url'];?>" type="audio/mpeg">
                </audio>
                <span class="file__title"><?php echo $file['title']; ?></span>
            </div>
        <?php endif;
        endif;
    endforeach; ?>
</div>