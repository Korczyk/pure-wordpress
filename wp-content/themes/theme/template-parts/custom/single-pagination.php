<div class="pagination-simple">
    <?php
    $the_query = isset($args['the_query'])&&$args['the_query']!=''?$args['the_query']:'';
    $total_pages = $the_query->max_num_pages;

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
    }

    ?>
    <?php if(isset($current_page)): ?>
        <?php
        if($current_page == 1){
            $prevPage = $current_page;
        }else{
            $prevPage = $current_page - 1;
        }
        ?>
        <a <?php if ($current_page <= 1){echo 'class="is-disabled"';} else {?> href="<?php echo get_post_type_archive_link($post_type) . "page/" . $prevPage . "/"?>" <?php } ?>>
            <span class="icon icon-icon-arrow-left-circle"></span>
        </a>
        <div class="pagination-middle">
            <input type="number" min="1" data-url="<?php echo get_post_type_archive_link($post_type); ?>" max="<?php echo $total_pages ?>" value="<?php echo $current_page; ?>">
            <div class="total_pages">
                <span class="from">z</span>
                <span class="to"><?php echo $total_pages ?></span>
            </div>
        </div>
        <?php
        $nextPage = $current_page + 1;
        if($nextPage >= $total_pages){
            $nextPage = $total_pages;
        }
        ?>
        <a <?php if ($current_page >= $total_pages){echo 'class="is-disabled"';} else {?> href="<?php echo get_post_type_archive_link($post_type) . "page/" . $nextPage  . "/"?>" <?php } ?>>
            <span class="icon icon-icon-arrow-right-circle"></span>
        </a>
    <?php endif; ?>
</div>