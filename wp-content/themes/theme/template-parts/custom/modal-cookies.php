<div class="modal fade" id="modal-cookies" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title header-2 mb-0">Informacje o cookies</h2>
            </div>
            <div class="modal-body">
                <?php dynamic_sidebar('cookies'); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" aria-label="Close" id="btn-accept">
                    <span class="btn-label">Rozumiem</span>
                </button>
            </div>
        </div>
    </div>
</div>