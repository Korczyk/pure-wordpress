<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'community';
?>
<?php $content = get_field($key);?>
<div class="box--grey">
<?php foreach ($content as $row): ?>
    <?php if(isset($row['trigger'])&&$row['trigger']): ?>
        <?php
        $title = $row['name'];
        $keeper = $row['keeper'];
        $text = $row['text'];
        ?>
        <div class="col-community">
            <h2 class="header-2"><?php echo isset($title)&&$title!=""?$title:'Nazwa grupy'; ?></h2>
            <?php if(isset($keeper)&&$keeper!=""): ?>
                <div class="text-elem"><span class="text-elem keeper">Opiekun: </span><?php echo $keeper; ?></div>
            <?php endif; ?>
            <?php if(isset($text)&&$text!=""): ?>
                <div class="list-elem"><?php echo $text; ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
<?php endforeach; ?>
</div>
