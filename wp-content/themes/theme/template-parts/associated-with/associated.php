<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'associated_with';
?>
<?php $content = get_field($key);?>

<?php if(isset($content['trigger'])&&$content['trigger']): ?>
    <div class="container__associated <?php echo $class; ?>">
        <div class="row">
            <div class="col">
                <h2 class="header-2 header--bg"><?php echo isset($content['label'])&&$content['label']!=""?$content['label']:null; ?></h2>
            </div>
        </div>
        <div class="row">
            <ul class="list-of-elem wysiwyg">
                <?php foreach ($content['persons'] as $elem): ?>
                    <li class="list-elem"><?php echo $elem['person']; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <br>
        <div class="row">
            <ul class="list-of-elem wysiwyg">
                <?php foreach ($content['other_persons'] as $elem): ?>
                    <li class="list-elem"><?php echo $elem['other_person']; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
