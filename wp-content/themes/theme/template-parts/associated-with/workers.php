<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'workers';
?>
<?php $content = get_field($key);?>

<?php if(isset($content['trigger'])&&$content['trigger']): ?>
    <div class="container__workers container__vertical-margin <?php echo $class; ?>">
        <div class="row">
            <div class="col">
                <h2 class="header-2 header--bg"><?php echo isset($content['label'])&&$content['label']!=""?$content['label']:null; ?></h2>
            </div>
        </div>
        <div class="row">
            <ul class="list-of-elem">
                <?php foreach ($content['worker'] as $elem): ?>
                    <li class="list-elem"><?php echo $elem['full_name']; ?> - <?php echo $elem['text']; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>
