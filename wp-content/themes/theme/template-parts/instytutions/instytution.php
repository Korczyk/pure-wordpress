<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'instytution';
?>
<?php $content = get_field($key);?>
<div class="box--grey">
    <?php foreach ($content as $row): ?>
        <?php if(isset($row['trigger'])&&$row['trigger']): ?>
            <?php
            $title = $row['title'];
            ?>
            <div class="col-instytution">
                <h2 class="header-2"><?php echo isset($title)&&$title!=""?$title:'Nazwa grupy'; ?></h2>
                <ul class="list-of-elem wysiwyg">
                    <?php foreach ($row['row'] as $elem): ?>
                        <li class="list-elem"><?php echo $elem['facility']; ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div>
