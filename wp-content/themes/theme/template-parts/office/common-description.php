<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:null;
?>
<?php $content = get_field($key);?>
<?php if(isset($content['trigger'])&&$content['trigger']): ?>
    <div class="container container-fluid <?php echo $class; ?>">
        <div class="row">
            <div class="col">
                <h2 class="header-2 header--bg"><?php echo isset($content['header'])&&$content['header']!=""?$content['header']:acf_get_field($key)['sub_fields'][0]['default_value']; ?></h2>
                <div class="text"><?php echo isset($content['text'])&&$content['text']!=""?$content['text']:null; ?></div>
            </div>
        </div>
    </div>
<?php endif; ?>