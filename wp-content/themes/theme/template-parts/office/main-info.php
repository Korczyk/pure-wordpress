<div class="container container-fluid">
    <div class="row">
        <div class="col">
            <?php $content = get_field('opening_hour');?>
            <?php if(isset($content['trigger'])&&$content['trigger']): ?>
                <div class="text header--bg"><?php echo isset($content['text'])&&$content['text']!=''?$content['text']:null; ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>