<?php $class = isset($args['class'])&&$args['class']!=''?$args['class']:null; ?>
<div class="row">
    <div class="col">
        <h1 class="header-1 text-center header--bg <?php echo $class; ?>"><?php echo get_the_title(); ?></h1>
    </div>
</div>