<?php
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'parish_priest';
$content = get_field($key)[0];


$header = $content['header'];
$fullname = $content['fullname'];
$image = $content['image'];
$text = $content['text'];
?>
<?php if(isset($content['trigger']) && $content['trigger']): ?>
<div class="container__parish-priest container__vertical-margin">
    <div class="row">
        <figure class="col-lg-4 col-md-4 col-image"><img class="lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo isset($image['url'])&&$image['url']!=''?$image['url']:get_events_image();?>" alt="<?php echo isset($image['alt'])&&$image['alt']!=''?$image['alt']:'image';?>"></figure>
        <div class="col-lg-8 col-md-8 col-text wysiwyg">
            <h3 class="header-2 header"><?php echo isset($header)&&$header!=''?$header:acf_get_field($key)['sub_fields'][0]['default_value'];?>: <?php echo isset($fullname)&&$fullname!=''?$fullname:null;?></h3>
            <?php echo isset($text)&&$text!=''?$text:null;?>
        </div>
    </div>
</div>
<?php endif; ?>