<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'vicars';
$content = get_field($key);
$label = $content['label'];

$person = $content['priest'];
?>
<?php if(isset($content['trigger'])&&$content['trigger']): ?>
<div class="container__vicars container__vertical-margin <?php echo $class; ?>">
    <div class="row">
        <div class="col">
            <h2 class="header-2 header--bg"><?php echo isset($label)&&$label!=""?$label:null; ?></h2>
        </div>
    </div>
    <div class="row vicars">
        <?php foreach ($person as $elem): ?>
            <?php
            $fullname = $elem['full_name'];
            if(isset($elem['image'])) {
                $image = $elem['image'];
            }

            $text = $elem['text'];
            ?>
            <div class="col-lg-4 vicar">
                <?php if(isset($elem['image'])): ?>
                <figure>
                    <img class="vicar__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo isset($image['url'])&&$image['url']!=''?$image['url']:get_events_image(); ?>; ?>" alt="<?php echo isset($image['alt'])&&$image['alt']!=''?$image['alt']:'image'; ?>">
                </figure>
                <?php endif; ?>
                <h3 class="header-3 vicar__fullname"><?php echo isset($fullname)&&$fullname!=""?$fullname:null; ?></h3>
                <div class="vicar__description"><?php echo isset($text)&&$text!=""?$text:null; ?></div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>