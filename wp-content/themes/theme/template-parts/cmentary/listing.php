<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'timeline';
?>
<?php $content = get_field($key);?>

<?php $counter = 1; ?>
<?php foreach ($content['row'] as $row): ?>
    <?php if(isset($row['trigger'])&&$row['trigger']): ?>
        <?php
        $text = $row['text'];
        $image = $row['image'];
        ?>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-text wysiwyg <?php if($counter%2==0): ?>order-0<?php else: ?>order-1<?php endif; ?>"><?php echo isset($text)&&$text!=''?$text:null; ?></div>
            <figure class="col-lg-6 col-md-6 col-image <?php if($counter%2==0): ?>order-1<?php else: ?>order-0<?php endif; ?>"><img class="margin-auto lazy" src="<?php echo get_blank_gif();?>" data-src="<?php echo isset($image)&&$image['url']!=''?$image['url']:null; ?>" alt="<?php echo isset($image)&&$image['alt']!=''?$image['alt']:'image'; ?>"></figure>
        </div>
        <?php $counter++; ?>
    <?php endif; ?>
<?php endforeach; ?>