<?php
$arr = explode_url();
if($arr[1] == PILGRIMAGE_URL): ?>
    <div class="widget social-content youtube">
        <div class="social-content__box">
            <a href="<?php echo get_youtube(); ?>" target="_blank" rel="nofollow" class="social-icon facebook"><img src="<?php echo get_images_dir().'/youtube.png' ?>"></a>
        </div>
    </div>
<?php endif; ?>
