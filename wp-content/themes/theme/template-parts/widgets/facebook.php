<?php
$arr = explode_url();
if($arr[1] != PILGRIMAGE_URL): ?>
    <div class="widget social-content facebook">
        <div class="social-content__box">
            <a href="<?php echo get_facebook(); ?>" target="_blank" rel="nofollow" class="social-icon facebook"><img src="<?php echo get_images_dir().'/facebook-f.png' ?>"></a>
        </div>
    </div>
<?php endif; ?>
