<div class="container container__vertical-margin container__pt">
    <div class="row justify-content-between">
        <div class="col-md-6 col-xl-6 pt">
            <h2 class="pt-name header-1">Ogłoszenia parafialne</h2>
            <ul class="timeline">
                <?php
                $post_type = 'parish_announcements';
                $query_options = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => 1,
                    'post__not_in' => array(get_the_ID())
                );
                $the_query = new WP_Query( $query_options );
                if($the_query -> have_posts()):
                    while ($the_query -> have_posts()) : $the_query -> the_post();
                        ?>
                        <li class="timeline-item bg-white rounded shadow">
                            <a href="<?php echo get_post_permalink( $post->$id); ?>" title="" class="timeline-item-link">
                                <div class="timeline-arrow"></div>
                                <h2 class="header-3 mb-0"><?php echo get_the_title(); ?></h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i></span>
                                <?php
                                    $text = get_field('text');
                                    if($text != ''):
                                ?>
                                <p class="text-small mt-2 font-weight-light"><?php echo preview($text,200); ?></p>
                                <?php endif; ?>
                            </a>
                        </li>
                    <?php
                    endwhile;
                else: ?>
                    <li class="timeline-item bg-white rounded shadow">Brak ogłoszeń parafialnych</li>
                <?php endif; wp_reset_postdata(); ?>
            </ul>
            <a href="<?php echo get_post_type_archive_link($post_type); ?>" class="btn"><span class="btn-label">Pokaż więcej</span></a>
        </div>
        <div class="col-md-6 col-xl-6 pt">
            <h2 class="pt-name header-1">Wydarzenia parafialne</h2>
            <ul class="timeline">
                <?php
                $post_type = 'events';
                $query_options = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',
                    'posts_per_page' => 2,
                    'post__not_in' => array(get_the_ID())
                );
                $the_query = new WP_Query( $query_options );
                if($the_query -> have_posts()):
                    while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                        <li class="timeline-item bg-white rounded shadow">
                            <a href="<?php echo get_post_permalink( $post->$id); ?>" title="" class="timeline-item-link">
                                <div class="timeline-arrow"></div>
                                <h2 class="header-3 mb-0"><?php echo get_the_title(); ?></h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i></span>
                                <?php
                                $text = get_field('text');
                                if($text != ''):
                                    ?>
                                    <p class="text-small mt-2 font-weight-light"><?php echo preview($text,200); ?></p>
                                <?php endif; ?>
                            </a>
                        </li>
                    <?php
                    endwhile; else:?>
                    <li class="timeline-item bg-white rounded shadow">Brak ogłoszeń parafialnych</li>
                <?php endif; wp_reset_postdata(); ?>
            </ul>
            <a href="<?php echo get_post_type_archive_link($post_type); ?>" class="btn"><span class="btn-label">Pokaż więcej</span></a>
        </div>
    </div>
</div>