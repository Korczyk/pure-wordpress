<div class="container container__vertical-margin container__pt">
    <div class="row justify-content-between">
        <div class="col-md-6 col-xl-6 pt">
            <?php /*
            <h2 class="pt-name header-1">Ogłoszenia parafialne</h2>
            <div class="swiper-container swiper__pt">
                <div class="swiper-wrapper">
                    <?php
                    $post_type = 'parish_announcements';
                    $query_options = array(
                        'post_type' => $post_type,
                        'post_status' => 'publish',
                        'posts_per_page' => 2,
                        'post__not_in' => array(get_the_ID())
                    );
                    $the_query = new WP_Query( $query_options );
                    if($the_query -> have_posts()):
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                            ?>
                            <div class="swiper-slide pt-item">
                                <a href="<?php echo get_post_permalink( $post->$id); ?>" title="" class="pt-link">
                                    <figure class="pt-item__image"><img class="lazy" src="<?php echo get_blank_gif();?>" data-src="<?php echo get_parish_announcement_image(); ?>"></figure>
                                    <div class="pt-item__header"><?php the_title(); ?></div>
                                </a>
                            </div>
                        <?php
                        endwhile;
                    else:
                        ?>
                        <div class="swiper-slide pt-item">Brak ogłoszeń parafialnych</div>
                    <?php
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
 */ ?>
            <?php /* <a href="<?php echo get_post_type_archive_link($post_type); ?>" class="btn"><span class="btn-label">Pokaż więcej</span></a>*/ ?>
                <!-- Timeline -->
            <h2 class="pt-name header-1">Ogłoszenia parafialne</h2>
            <ul class="timeline">
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 1</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>21 March, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 2</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>5 April, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper.</p>
                    <p class="text-small mt-2 font-weight-light">Libero expedita explicabo eius fugiat quia aspernatur autem laudantium error architecto recusandae natus sapiente sit nam eaque, consectetur porro molestiae ipsam! Deleniti.</p>
                </li>
            </ul><!-- End -->
        </div>
        <div class="col-md-6 col-xl-6 pt">
            <?php /*
            <?php $link = null; ?>
            <h2 class="pt-name header-1">Wydarzenia parafialne</h2>
            <div class="swiper-container swiper__pt">
                <div class="swiper-wrapper">
                    <?php
                    $post_type = 'events';
                    $query_options = array(
                        'post_type' => $post_type,
                        'post_status' => 'publish',
                        'posts_per_page' => 20,
                        'post__not_in' => array(get_the_ID())
                    );
                    $the_query = new WP_Query( $query_options );
                    if($the_query -> have_posts()):
                        while ($the_query -> have_posts()) : $the_query -> the_post();
                            ?>
                            <div class="swiper-slide pt-item">
                                <a href="<?php echo get_post_permalink( $post->$id); ?>" title="" class="pt-link">
                                    <?php if(has_post_thumbnail()): ?>
                                        <figure class="pt-item__image"><img class="elem__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image"></figure>
                                    <?php else: ?>
                                        <figure class="pt-item__image"><img class="elem__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo get_events_image(); ?>" alt="image"></figure>
                                    <?php endif; ?>
                                    <div class="pt-item__header"><?php the_title(); ?></div>
                                </a>
                            </div>
                        <?php
                        endwhile;
                    else:
                        ?>
                        <div class="swiper-slide pt-item">Brak wydarzeń</div>
                    <?php
                    endif;
                    wp_reset_postdata();
                    ?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            */ ?>
            <?php /*<a href="<?php echo get_post_type_archive_link($post_type); ?>" class="btn"><span class="btn-label">Pokaż więcej</span></a> */ ?>

            <h2 class="pt-name header-1">Wydarzenia parafialne</h2>
            <ul class="timeline">
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 1</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>21 March, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 2</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>5 April, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper.</p>
                    <p class="text-small mt-2 font-weight-light">Libero expedita explicabo eius fugiat quia aspernatur autem laudantium error architecto recusandae natus sapiente sit nam eaque, consectetur porro molestiae ipsam! Deleniti.</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 3</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>18 August, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                </li>
                <li class="timeline-item bg-white rounded ml-3 p-4 shadow">
                    <div class="timeline-arrow"></div>
                    <h2 class="h5 mb-0">Title of section 4</h2><span class="small text-gray"><i class="fa fa-clock-o mr-1"></i>10 October, 2019</span>
                    <p class="text-small mt-2 font-weight-light">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque scelerisque diam non nisi semper, et elementum lorem ornare. Maecenas placerat facilisis mollis. Duis sagittis ligula in sodales vehicula....</p>
                    <p class="text-small mt-2 font-weight-light">Voluptatibus temporibus esse illum eum aspernatur, fugiat suscipit natus! Eum corporis illum nihil officiis tempore. Excepturi illo natus libero sit doloremque, laborum molestias rerum pariatur quam ipsam necessitatibus incidunt, explicabo.</p>
                </li>
            </ul><!-- End -->
        </div>
    </div>
</div>


<style>
    /* Timeline holder */
    ul.timeline {
        list-style-type: none;
        position: relative;
        padding-left: 1.5rem;
    }

    /* Timeline vertical line */
    ul.timeline:before {
        content: ' ';
        background: #fff;
        display: inline-block;
        position: absolute;
        left: 16px;
        width: 4px;
        height: 100%;
        z-index: 400;
        border-radius: 1rem;
    }

    li.timeline-item {
        margin: 20px 0;
    }

    /* Timeline item arrow */
    .timeline-arrow {
        border-top: 0.5rem solid transparent;
        border-right: 0.5rem solid #fff;
        border-bottom: 0.5rem solid transparent;
        display: block;
        position: absolute;
        left: 2rem;
    }

    /* Timeline item circle marker */
    li.timeline-item::before {
        content: ' ';
        background: #ddd;
        display: inline-block;
        position: absolute;
        border-radius: 50%;
        border: 3px solid #fff;
        left: 11px;
        width: 14px;
        height: 14px;
        z-index: 400;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.2);
    }


</style>