<?php $slider = get_field('main_banner_group');?>

    <?php if(isset($slider['trigger'])&&$slider['trigger']): ?>
    <div class="container-fluid container__vertical-margin container__margin-top-0 container__main-banner">
        <div class="row">
            <div class="swiper-container  swiper__main-banner">
                <div class="swiper-wrapper">
                    <?php foreach ($slider['main_banner'] as $slide): ?>

                        <picture class="swiper-slide" >
                            <source media="(max-width:768px)" srcset="<?php echo $slide['image_mobile']['url']?>">
                            <source media="(min-width:769px)" srcset="<?php echo $slide['image']['url']; ?>">
                            <img src="<?php echo $slide['image']['url']; ?>" alt="banner">
                            <!--                            <img srcset="--><?php //echo $slide['image_mobile']['url']?><!-- 768w, --><?php //echo $slide['image']['url']; ?><!--" sizes="(max-width: 768px)" src="--><?php //echo $slide['image']['url']; ?><!--" alt="banner">-->
                        </picture>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>
    </div>
<?php endif; ?>