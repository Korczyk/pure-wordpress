<?php
$content = isset($args['key'])&&$args['key']!=''?$args['key']:null;
?>
<div class="container-fluid container__vertical-margin container__timeline">
    <div class="container">
        <div class="row">
            <?php /* https://codyhouse.co/gem/horizontal-timeline */ ?>
            <?php if(isset($content['time_line']['event'])): ?>
                <section class="cd-h-timeline js-cd-h-timeline margin-bottom-md timeline">
                    <h2 class="timeline__header header-1"><?php echo isset($content['time_line']['header'])&&$content['time_line']['header']!=""?$content['time_line']['header']:acf_get_field('time_line')['sub_fields'][0]['default_value']; ?></h2>
                    <div class="cd-h-timeline__container container">
                        <div class="cd-h-timeline__dates">
                            <div class="cd-h-timeline__line">
                                <ol>
                                    <?php $flag = true; ?>
                                    <?php foreach ($content['time_line']['event'] as $event): ?>
                                        <?php $date = $event['date']; ?>
                                        <li><a href="#0" data-date="00:00" class="cd-h-timeline__date <?php if($flag): ?>cd-h-timeline__date--selected<?php endif; ?>"><?php echo $date ?></a></li>
                                        <?php if($flag) $flag = false; ?>
                                    <?php endforeach; ?>
                                </ol>
                                <span class="cd-h-timeline__filling-line" aria-hidden="true"></span>
                            </div>
                        </div>
                        <ul>
                            <li><a href="#0" class="text-replace cd-h-timeline__navigation cd-h-timeline__navigation--prev cd-h-timeline__navigation--inactive">Prev</a></li>
                            <li><a href="#0" class="text-replace cd-h-timeline__navigation cd-h-timeline__navigation--next">Next</a></li>
                        </ul>
                    </div>

                    <div class="cd-h-timeline__events">
                        <ol>
                            <?php $flag = true; ?>
                            <?php foreach ($content['time_line']['event'] as $event): ?>
                                <?php $date = $event['date']; ?>
                                <?php $description = $event['description']; ?>
                                <li class="cd-h-timeline__event text-component<?php if($flag): ?>cd-h-timeline__event--selected<?php endif; ?>">
                                    <div class="cd-h-timeline__event-content container">
                                        <div class="cd-h-timeline__event-date"><?php echo $date ?></div>
                                        <p class="cd-h-timeline__event-description color-contrast-medium"><?php echo $description ?></p>
                                    </div>
                                </li>
                                <?php if($flag) $flag = false; ?>
                            <?php endforeach; ?>
                        </ol>
                    </div>
                </section>
            <?php endif; ?>
        </div>
    </div>
</div>