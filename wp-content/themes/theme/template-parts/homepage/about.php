<?php $about_us = get_field('about_us');?>
<?php if(isset($about_us['trigger'])&&$about_us['trigger']): ?>
    <div class="container-fluid container__vertical-margin container__about-us">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-991-padding-auto-0"><img class="lazy main-image" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo isset($about_us['image'])?$about_us['image']['url']:null; ?>" alt="image"></div>
                <div class="col-lg-7 col-991-padding-auto-0">
                    <?php if(isset($about_us['text']['section'])): ?>
                        <ul class="list-of-info">
                            <?php foreach ($about_us['text']['section'] as $section): ?>
                                <li class="info">
                                    <?php if(isset($section['title'])&&$section['title']!=''): ?>
                                        <h3 class="title header-1"><?php echo $section['title']; ?></h3>
                                    <?php endif; ?>
                                    <?php if(isset($section['paragraph'])&&$section['paragraph']!=''): ?>
                                        <div class="paragraph"><?php echo $section['paragraph']; ?></div>
                                    <?php endif; ?>
                                    <?php if(isset($section['image'])&&$section['image']['url']!=''): ?>
                                        <figure>
                                            <img class="image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo $section['image']['url']; ?>" alt="alt">
                                        </figure>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php get_template_part('template-parts/homepage/timeline',null,['key'=>$about_us]); ?>
<?php endif; ?>

