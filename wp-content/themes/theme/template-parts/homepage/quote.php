<?php $quote = get_field('quote');?>
<?php if(isset($quote['trigger'])&&$quote['trigger']): ?>
    <div class="container-fluid container__vertical-margin container__quote">
        <div class="container">
            <div class="row">
                <div class="col-12 col-991-padding-auto-0">
                    <h2 class="quote">"<?php echo isset($quote['text'])&&$quote['text']!=""?$quote['text']:acf_get_field('quote')['sub_fields'][0]['default_value']; ?>"</h2>
                    <p class="author"><?php echo isset($quote['author'])&&$quote['author']!=""?$quote['author']:acf_get_field('quote')['sub_fields'][1]['default_value']; ?></p>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>