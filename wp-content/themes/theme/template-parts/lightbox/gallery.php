<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:'gallery';
?>
<?php $content = get_field($key);?>
<?php $counter = 0; ?>
<?php foreach ($content as $row): ?>
    <?php
        $main_image = $row['main_image'];
        $label = $row['label'];
        $images = $row['images'];
    ?>
    <?php if(isset($row['trigger'])&&$row['trigger']): ?>
    <div class="col-lg-4 col-md-6 gallery">
        <a href="<?php echo isset($main_image['url'])&&$main_image['url']?$main_image['url']:get_events_image(); ?>" data-lightbox="gallery-<?php echo $counter; ?>" class="gallery__preview">
            <h3 class="header-3 gallery__label" data-lightbox="gallery-<?php echo $counter; ?>"><?php echo isset($label)&&$label?$label:'Zobacz galerię'; ?></h3>
            <figure class="box-shadow">
                <img class="gallery__main-image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo isset($main_image['url'])&&$main_image['url']?$main_image['url']:get_events_image(); ?>" alt="<?php echo isset($main_image['alt'])&&$main_image['alt']?$main_image['alt']:'image'; ?>">
            </figure>
        </a>
        <?php if(count($images)): ?>
            <?php foreach ($images as $elem): ?>
            <?php $image = $elem['image']; ?>
            <?php $description = $elem['description']; ?>
                <a data-lightbox="gallery-<?php echo $counter; ?>" href="<?php echo isset($image['url'])&&$image['url']?$image['url']:get_events_image(); ?>" data-title="<?php echo isset($description)&&$description?$description:null; ?>"></a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <?php $counter++; ?>
    <?php endif ;?>
<?php endforeach; ?>