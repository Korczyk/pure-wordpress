<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:null;
?>
<?php $content = get_field($key);?>
<?php if(isset($content['trigger'])&&$content['trigger']): ?>
    <div class="container container-fluid <?php echo $class; ?>">
        <div class="row">
            <div class="col">
                <h2 class="header-2 header--bg"><?php echo isset($content['header'])&&$content['header']!=""?$content['header']:acf_get_field($key)['sub_fields'][0]['default_value']; ?></h2>
                <div class="table-box">
                    <div class="table-max-width">
                        <table class="table">
                            <?php
                            $hour_earlier = $content['hours']['hour_earlier'];
                            $hour_later = $content['hours']['hour_later'];
                            ?>
                            <tr class="t-row"><th class="t-cell header-2">Dzień tygodnia</th><th class="t-cell header-2"><?php echo $hour_earlier; ?></th><th class="t-cell header-2"><?php echo $hour_later; ?></th></tr>
                            <?php foreach($content['duty'] as $duty): ?>
                                <?php
                                $priest_earlier = $duty['priest_earlier'];
                                $priest_later = $duty['priest_later'];
                                $day = $duty['day_name'];
                                if($day && $priest_earlier && $priest_later):
                                    ?>
                                    <tr class="t-row"><td class="t-cell"><?php echo $day; ?></td><td class="t-cell"><?php echo $priest_earlier; ?></td><td class="t-cell"><?php echo $priest_later; ?></td></tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
                <?php if(isset($content['added_table_text'])&&$content['added_table_text']):?>
                    <div class="wysiwyg">
                        <p class="added-info"><?php echo $content['added_table_text']; ?></p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>