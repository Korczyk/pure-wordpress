<?php
$class = isset($args['class'])&&$args['class']!=''?$args['class']:null;
$key = isset($args['key'])&&$args['key']!=''?$args['key']:null;
?>
<?php $content = get_field($key);?>
<?php if(isset($content['trigger'])&&$content['trigger']): ?>
    <div class="container container-fluid <?php echo $class; ?>">
        <div class="row">
            <div class="col">
                <h2 class="header-2 header--bg"><?php echo isset($content['header'])&&$content['header']!=""?$content['header']:acf_get_field($key)['sub_fields'][0]['default_value']; ?></h2>
                <ul class="list">
                    <?php foreach ($content['records'] as $record): ?>
                        <?php
                        $header = $record['header'];
                        $text = $record['text'];
                        ?>
                        <li class="elem">
                            <h3 class="header"><?php echo $header!=''?$header:null ?></h3>
                            <div class="text"><?php echo $text!=''?$text:null ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<?php endif; ?>