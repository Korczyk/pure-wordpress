<div class="row">
    <div class="col-12">
        <div class="breadcrumbs-wrapper">
            <div class="breadcrumbs box--grey">
                <?php
                if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<p>','</p>' );
                }
                ?>
            </div>
        </div>
    </div>
</div>