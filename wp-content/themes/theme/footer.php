<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
            <?php
            if ( function_exists('dynamic_sidebar') || dynamic_sidebar('cookies') ) :
                get_template_part('template-parts/custom/modal-cookies');
            endif;
            ?>
            <div class="info-bar">
                <div class="info-bar__internal">
                    <div class="info-bar__container js-info-bar">
                        <?php
                            if ( function_exists('dynamic_sidebar') || dynamic_sidebar('info-bar') ) :
                                dynamic_sidebar('info-bar');
                            endif;
                        ?>
                    </div>
                </div>
            </div>
			<footer id="site-footer" role="contentinfo" class="header-footer-group container-fluid">
                <div class="container">
                    <?php  if ( function_exists('dynamic_sidebar') || dynamic_sidebar('footer') ) :
                        dynamic_sidebar('footer');
                    endif; ?>
                </div>
			</footer><!-- #site-footer -->

            <div class="cta-to-top box-shadow">
<!--                <img src="--><?php //echo get_cat_to_top(); ?><!--" alt="image">-->
            </div>
            <div class="cta-to-right"><span></span><span></span><span></span></div>

		<?php wp_footer(); ?>
	</body>
</html>
