<?php
/**
 *
 * Template name: Szablon Liturgia na dziś
 *
 * @package storefront
 */
get_header(); ?>
<div class="template" id="template_liturgy">
    <div class="container">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <div class="row">
            <div class="col">
                <h1 class="header-1  header--bg text-center"><?php echo get_the_title(); ?></h1>
            </div>
        </div>
        <div class="row box--grey">
            <div class="col-12" id="liturgy-text">
                <h2 class="header-2">Przeczytaj</h2>
                <script type="text/javascript" src="https://widget.niedziela.pl/liturgia_out.js.php"></script>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
