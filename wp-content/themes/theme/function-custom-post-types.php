<?php
// https://generatewp.com/post-type/
//https://www.kevinleary.net/wordpress-dashicons-list-custom-post-type-icons/

// Register Custom Post Type

/*
function cpt_parish_announcements() {
    $labels = array(
        'name'                  => _x( 'Ogłoszenia parafialne', 'Post Type General Name', 'am' ),
        'singular_name'         => _x( 'Ogłoszenia parafialne', 'Post Type Singular Name', 'am' ),
        'menu_name'             => __( 'Ogłoszenia parafialne', 'am' ),
        'name_admin_bar'        => __( 'Post Type', 'am' ),
        'archives'              => __( 'Item Archives', 'am' ),
        'attributes'            => __( 'Item Attributes', 'am' ),
        'parent_item_colon'     => __( 'Parent Item:', 'am' ),
        'all_items'             => __( 'Wszystkie', 'am' ),
        'add_new_item'          => __( 'Dodaj nowy artykuł', 'am' ),
        'add_new'               => __( 'Dodaj nowy', 'am' ),
        'new_item'              => __( 'Dodaj nowy', 'am' ),
        'edit_item'             => __( 'Edytuj', 'am' ),
        'update_item'           => __( 'Zaktualizuj', 'am' ),
        'view_item'             => __( 'View Item', 'am' ),
        'view_items'            => __( 'View Items', 'am' ),
        'search_items'          => __( 'Search Item', 'am' ),
        'not_found'             => __( 'Not found', 'am' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'am' ),
        'featured_image'        => __( 'Featured Image', 'am' ),
        'set_featured_image'    => __( 'Set featured image', 'am' ),
        'remove_featured_image' => __( 'Remove featured image', 'am' ),
        'use_featured_image'    => __( 'Use as featured image', 'am' ),
        'insert_into_item'      => __( 'Insert into item', 'am' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'am' ),
        'items_list'            => __( 'Items list', 'am' ),
        'items_list_navigation' => __( 'Items list navigation', 'am' ),
        'filter_items_list'     => __( 'Filter items list', 'am' ),
    );
    $args = array(
        'label'                 => __( 'Ogłoszenia parafialne', 'am' ),
        'description'           => __( 'Post Type Description', 'am' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-format-aside',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'rewrite' => array(
            'with_front' => false,
            'slug' => 'ogloszenia'
        )

    );
    register_post_type( 'parish_announcements', $args );
}

function cpt_events() {
    $labels = array(
        'name'                  => _x( 'Wydarzenia parafialne', 'Post Type General Name', 'am' ),
        'singular_name'         => _x( 'Wydarzenia parafialne', 'Post Type Singular Name', 'am' ),
        'menu_name'             => __( 'Wydarzenia parafialne', 'am' ),
        'name_admin_bar'        => __( 'Post Type', 'am' ),
        'archives'              => __( 'Item Archives', 'am' ),
        'attributes'            => __( 'Item Attributes', 'am' ),
        'parent_item_colon'     => __( 'Parent Item:', 'am' ),
        'all_items'             => __( 'Wszystkie', 'am' ),
        'add_new_item'          => __( 'Dodaj nowy artykuł', 'am' ),
        'add_new'               => __( 'Dodaj nowy', 'am' ),
        'new_item'              => __( 'Dodaj nowy', 'am' ),
        'edit_item'             => __( 'Edytuj', 'am' ),
        'update_item'           => __( 'Zaktualizuj', 'am' ),
        'view_item'             => __( 'View Item', 'am' ),
        'view_items'            => __( 'View Items', 'am' ),
        'search_items'          => __( 'Search Item', 'am' ),
        'not_found'             => __( 'Not found', 'am' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'am' ),
        'featured_image'        => __( 'Featured Image', 'am' ),
        'set_featured_image'    => __( 'Set featured image', 'am' ),
        'remove_featured_image' => __( 'Remove featured image', 'am' ),
        'use_featured_image'    => __( 'Use as featured image', 'am' ),
        'insert_into_item'      => __( 'Insert into item', 'am' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'am' ),
        'items_list'            => __( 'Items list', 'am' ),
        'items_list_navigation' => __( 'Items list navigation', 'am' ),
        'filter_items_list'     => __( 'Filter items list', 'am' ),
    );
    $args = array(
        'label'                 => __( 'Wydarzenia parafialne', 'am' ),
        'description'           => __( 'Post Type Description', 'am' ),
        'labels'                => $labels,
        'supports'              => array( 'title','thumbnail'),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 32,
        'menu_icon'             => 'dashicons-calendar',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'rewrite' => array(
            'with_front' => false,
            'slug' => 'wydarzenia'
        )

    );
    register_post_type( 'events', $args );
}

function cpt_mass_intentions() {
    $labels = array(
        'name'                  => _x( 'Intencje mszalne', 'Post Type General Name', 'am' ),
        'singular_name'         => _x( 'Intencje mszalne', 'Post Type Singular Name', 'am' ),
        'menu_name'             => __( 'Intencje mszalne', 'am' ),
        'name_admin_bar'        => __( 'Post Type', 'am' ),
        'archives'              => __( 'Item Archives', 'am' ),
        'attributes'            => __( 'Item Attributes', 'am' ),
        'parent_item_colon'     => __( 'Parent Item:', 'am' ),
        'all_items'             => __( 'Wszystkie', 'am' ),
        'add_new_item'          => __( 'Dodaj nowy artykuł', 'am' ),
        'add_new'               => __( 'Dodaj nowy', 'am' ),
        'new_item'              => __( 'Dodaj nowy', 'am' ),
        'edit_item'             => __( 'Edytuj', 'am' ),
        'update_item'           => __( 'Zaktualizuj', 'am' ),
        'view_item'             => __( 'View Item', 'am' ),
        'view_items'            => __( 'View Items', 'am' ),
        'search_items'          => __( 'Search Item', 'am' ),
        'not_found'             => __( 'Not found', 'am' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'am' ),
        'featured_image'        => __( 'Featured Image', 'am' ),
        'set_featured_image'    => __( 'Set featured image', 'am' ),
        'remove_featured_image' => __( 'Remove featured image', 'am' ),
        'use_featured_image'    => __( 'Use as featured image', 'am' ),
        'insert_into_item'      => __( 'Insert into item', 'am' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'am' ),
        'items_list'            => __( 'Items list', 'am' ),
        'items_list_navigation' => __( 'Items list navigation', 'am' ),
        'filter_items_list'     => __( 'Filter items list', 'am' ),
    );
    $args = array(
        'label'                 => __( 'Intencje mszalne', 'am' ),
        'description'           => __( 'Post Type Description', 'am' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 33,
        'menu_icon'             => 'dashicons-pressthis',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'rewrite' => array(
            'with_front' => false,
            'slug' => 'intencje-mszalne'
        )

    );
    register_post_type( 'mass_intentions', $args );
}

function cpt_audiotec() {
    $labels = array(
        'name'                  => _x( 'Audioteka', 'Post Type General Name', 'am' ),
        'singular_name'         => _x( 'Audioteka', 'Post Type Singular Name', 'am' ),
        'menu_name'             => __( 'Audioteka', 'am' ),
        'name_admin_bar'        => __( 'Post Type', 'am' ),
        'archives'              => __( 'Item Archives', 'am' ),
        'attributes'            => __( 'Item Attributes', 'am' ),
        'parent_item_colon'     => __( 'Parent Item:', 'am' ),
        'all_items'             => __( 'Wszystkie', 'am' ),
        'add_new_item'          => __( 'Dodaj nowy artykuł', 'am' ),
        'add_new'               => __( 'Dodaj nowy', 'am' ),
        'new_item'              => __( 'Dodaj nowy', 'am' ),
        'edit_item'             => __( 'Edytuj', 'am' ),
        'update_item'           => __( 'Zaktualizuj', 'am' ),
        'view_item'             => __( 'View Item', 'am' ),
        'view_items'            => __( 'View Items', 'am' ),
        'search_items'          => __( 'Search Item', 'am' ),
        'not_found'             => __( 'Not found', 'am' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'am' ),
        'featured_image'        => __( 'Featured Image', 'am' ),
        'set_featured_image'    => __( 'Set featured image', 'am' ),
        'remove_featured_image' => __( 'Remove featured image', 'am' ),
        'use_featured_image'    => __( 'Use as featured image', 'am' ),
        'insert_into_item'      => __( 'Insert into item', 'am' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'am' ),
        'items_list'            => __( 'Items list', 'am' ),
        'items_list_navigation' => __( 'Items list navigation', 'am' ),
        'filter_items_list'     => __( 'Filter items list', 'am' ),
    );
    $args = array(
        'label'                 => __( 'Audioteka', 'am' ),
        'description'           => __( 'Post Type Description', 'am' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 34,
        'menu_icon'             => 'dashicons-format-video',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'rewrite' => array(
            'with_front' => false,
            'slug' => 'pielgrzymka-sieradzka/audioteka'
        )

    );
    register_post_type( 'audiotec', $args );
}


// ENABLE below custom post
function delete_post_type(){
    unregister_post_type( 'cpt_audiotec' );
}
add_action( 'init', 'cpt_parish_announcements', 1 );
add_action( 'init', 'cpt_events', 1 );
add_action( 'init', 'cpt_audiotec', 1 );


function toolset_fix_custom_posts_per_page( $query_string ){
    if( is_admin() || ! is_array( $query_string ) )
        return $query_string;

    $post_types = array('parish_announcements','events', 'mass_intentions', 'audiotec');

    foreach ($post_types as $pt) {
        $post_types_to_fix = array(
            array(
                'post_type' => $pt,
                'posts_per_page' => 2
            ),
        );

        foreach( $post_types_to_fix as $fix ) {
            if( array_key_exists( 'post_type', $query_string )
                && $query_string['post_type'] == $fix['post_type']
            ) {
                $query_string['posts_per_page'] = $fix['posts_per_page'];
                return $query_string;
            }
        }
    }
    return $query_string;
}
add_filter( 'request', 'toolset_fix_custom_posts_per_page' );
*/