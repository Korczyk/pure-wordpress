<?php

function wpb_init_widget_cookies($id) {
    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
            'name' => 'Cookies',
            'id' => 'cookies',
            'before_widget' => '<div class="text">',
            'after_widget' => '</div>',
        ));
    }
}
add_action('widgets_init','wpb_init_widget_cookies');

function wpb_init_widget_footer($id) {
    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
            'name' => 'Stopka',
            'id' => 'footer',
            'before_widget' => '<div class="footer-internal row">',
            'after_widget' => '</div>',
            'before_title' => '</h4>',
            'after_title' => '</h4>'
        ));
    }
}
add_action('widgets_init','wpb_init_widget_footer');


function wpb_init_widget_info_bar($id) {
    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
            'name' => 'Pasek informacyjny',
            'id' => 'info-bar',
            'before_widget' => '<span class="info-bar__info">',
            'after_widget' => '</span>',
        ));
    }
}
add_action('widgets_init','wpb_init_widget_info_bar');



function website_remove($fields)
{
    if(isset($fields['url']))
        unset($fields['url']);
    if(isset($fields['cookies']))
        unset($fields['cookies']);
    if(isset($fields['email']))
        unset($fields['email']);

    return $fields;
}
add_filter('comment_form_default_fields', 'website_remove');


add_filter('comment_form_defaults', 'set_my_comment_title', 20);
function set_my_comment_title( $defaults ){
    $defaults['title_reply'] = __('Wpisz się do księgi gości', 'customizr-child');
    $defaults['submit'] = __('Wpisz się', 'customizr-child');
    return $defaults;
}

function wpsites_modify_comment_form_email($arg) {
    $arg['author'] = '<p class="comment-form-author"><label for="author">' . _x( 'Imię', 'noun' ) . '</label><input id="author" name="author" required="true" type="text" value="" size="30" maxlength="245"></input></p>';
    return $arg;
}
add_filter('comment_form_default_fields', 'wpsites_modify_comment_form_email');


function wpsites_modify_comment_form_comment($arg) {
    $arg['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . _x( 'Treść', 'noun' ) . '</label><textarea id="comment" name="comment" cols="45" rows="1" aria-required="true"></textarea></p>';
    return $arg;
}
add_filter('comment_form_defaults', 'wpsites_modify_comment_form_comment');