<?php
/**
 * @package storefront
 */
get_header(); ?>
    <div class="template" id="template__mass-intention">
    <div class="guide-post">
        <div class="container container__vertical-margin container__margin-top-0 container-content-grey">
            <?php get_template_part('template-parts/breadcrumbs'); ?>
            <div class="row">
                <div class="col">
                    <div class="guide-post__wrapper">
                        <div class="guide-post__content" itemprop="articleBody">
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                                <h1 class="pt__header header-1 text-center header--bg">Intencja mszalne na okres <?php echo get_the_title(); ?></h1>
                                <?php $mass_intentions = get_field('mass_intentions'); ?>
                                <div class="pt__text intentions-tab tab">
                                <?php foreach ($mass_intentions as $mi): ?>
                                    <div class="tab__header header--bg header-2"><?php echo isset($mi['date'])&&$mi['date']!=''?$mi['date']:null ?><?php echo isset($mi['event'])&&$mi['event']!=''?"<br>".$mi['event']:null ?></div>
                                <?php foreach ($mi as $day): ?>
                                    <?php foreach ($day as $intention): ?>
                                    <div class="intention-tab__row tab__row wysiwyg">
                                        <div class="intention-tab__hour"><?php echo isset($intention['hour'])&&$intention['hour']!=''?$intention['hour']:null ?></div>
                                        <div class="intention-tab__label"><?php echo isset($intention['intention'])&&$intention['intention']!=''?$intention['intention']:null ?></div>
                                    </div>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                                <?php endforeach; ?>
                                </div>
                            <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>