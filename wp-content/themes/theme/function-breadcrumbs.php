<?php
add_filter( 'wpseo_breadcrumb_single_link' ,'wpseo_remove_breadcrumb_link', 10 ,2);

function wpseo_remove_breadcrumb_link( $link_output , $link ){
    $text_to_remove = 'Parafia';

    if( $link['text'] == $text_to_remove ) {
        $link_output = '';
    }

    return $link_output;
}