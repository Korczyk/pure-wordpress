<?php
get_header();
?>
<div class="template">
    <div class="container archive">
        <?php get_template_part('template-parts/breadcrumbs'); ?>
        <div class="row">
            <div class="col">
                <h1 class="pt__header header-1 text-center header--bg"><?php echo post_type_archive_title(); ?></h1>
            </div>
        </div>
        <div class="row list-of-elem">
            <?php
            $ppp = 6;
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $query_options = array(
                'post_type'=>'mass_intentions',
                'post_status'=>'publish',
                'posts_per_page'=>$ppp,
                'orderby'   => array(
                    'date' =>'DESC',
                ),
                'paged'=>$paged,
            );
            $the_query = new WP_Query( $query_options );
            ?>
            <?php if ( $the_query->have_posts() ) : ?>
                <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="box-shadow">
                            <a class="elem" href="<?php the_permalink(); ?>" title="<?php the_permalink(); ?>">
                                <img class="elem__image lazy" src="<?php echo get_blank_gif(); ?>" data-src="<?php echo get_mass_intention_image(); ?>" alt="image">
                                <h2 class="elem__header header-2"><?php the_title(); ?></h2>
                            </a>
                        </div>
                    </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php else : ?>
                <p><?php esc_html_e( 'Nie ma jeszcze postów.', 'kolegiata' ); ?></p>
            <?php endif; ?>
        </div>
        <?php if($the_query->found_posts >= $ppp): ?>
        <div class="row">
            <div class="col">
                <?php get_template_part('template-parts/custom/single-pagination',null,['the_query'=>$the_query]); ?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>
